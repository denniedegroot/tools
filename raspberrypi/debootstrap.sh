#!/bin/bash

# apt-get install binfmt-support qemu qemu-user-static debootstrap kpartx lvm2 dosfstools

deb_release="wheezy"
deb_mirror="http://archive.raspbian.org/raspbian"

bootsize="64"
imagesize="1024"

buildenv="${HOME}/raspberrypi.image"
rootfs="${buildenv}/rootfs"
bootfs="${rootfs}/boot"

device=$1
image=""

# Preflight checks
if [ $EUID -ne 0 ]; then
  echo "This tool must be run as root"
  exit 1
fi

if [ "${device}" == "" ]; then
  echo "usage: give device parameter like /dev/sdx or image"
  exit 1
fi

if [ "${device}" == "image" ]; then
  echo "Creating an image"
  mkdir -p ${buildenv}
  image="${buildenv}/rpi_armhf_${deb_release}.img"
  dd if=/dev/zero of=${image} bs=1MB count=${imagesize}
  device=`losetup -f --show ${image}`
  echo "Image ${image} created and mounted as ${device}"
else
  if ! [ -b ${device} ]; then
    echo "${device} is not a block device"
    exit 1
  fi
  umount ${device}*
  dd if=/dev/zero of=${device} bs=512 count=1
fi

# Create partitions
fdisk ${device} << EOF
n
p
1

+${bootsize}M
t
c
n
p
2


w
EOF

if [ "${image}" != "" ]; then
  losetup -d ${device}
  device=`kpartx -va ${image} | sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1`
  device="/dev/mapper/${device}"
  bootp=${device}p1
  rootp=${device}p2
else
  if ! [ -b ${device}1 ]; then
    bootp=${device}p1
    rootp=${device}p2
    if ! [ -b ${bootp} ]; then
      echo "Can't find bootpartition neither as ${device}1 nor as ${device}p1, exiting."
      exit 1
    fi
  else
    bootp=${device}1
    rootp=${device}2
  fi  
fi

mkfs.vfat ${bootp} -n boot
mkfs.ext4 ${rootp} -L rootfs

mkdir -p ${rootfs}
mount ${rootp} ${rootfs}
mkdir -p ${bootfs}
mount ${bootp} ${bootfs}

sleep 5

# Create the filesystem
cd ${rootfs}

echo "dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait" > boot/cmdline.txt

echo "arm_freq=850
sdram_freq=400
over_voltage=2
gpu_mem=16" > boot/config.txt

debootstrap --no-check-gpg --foreign --arch armhf ${deb_release} ${rootfs} ${deb_mirror}
cp /usr/bin/qemu-arm-static usr/bin/

LANG=C chroot ${rootfs} /debootstrap/debootstrap --second-stage

echo "deb ${deb_mirror} ${deb_release} main
deb http://apt.denniedegroot.nl/pi ${deb_release} main
" > etc/apt/sources.list

wget http://archive.raspbian.org/raspbian.public.key -O - | apt-key add -

echo "proc            /proc           proc    defaults        0       0
/dev/mmcblk0p1  /boot           vfat    defaults        0       0
" > etc/fstab

echo "raspberrypi" > etc/hostname
echo "/opt/vc/lib" > etc/ld.so.conf.d/vmcs.conf
echo "PATH=/opt/vc/bin:/opt/vc/sbin:\$PATH" > etc/profile.d/rpi-path.sh

echo "auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet dhcp
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" > etc/network/interfaces

echo "vchiq
snd_bcm2835
" >> etc/modules

mv etc/sysctl.conf etc/sysctl.bak
sed '/^#kernel.printk/ c\kernel.printk = 3 4 1 3' etc/sysctl.bak > etc/sysctl.conf
rm etc/sysctl.bak

echo "vm.min_free_kbytes = 8192
vm.swappiness=1" >> etc/sysctl.conf

echo "#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will \"exit 0\" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# Enable Turbo Mode @ 60% usage
echo \"ondemand\" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo 60 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold

# Disable PAL/HDMI output
/opt/vc/bin/tvservice -off

exit 0" > etc/rc.local

echo "# Disable power saving
options 8192cu rtw_power_mgnt=0 rtw_enusbss=1 rtw_ips_mode=1
" > etc/modprobe.d/8192cu.conf

echo "#!/bin/bash
wget http://apt.denniedegroot.nl/apt.denniedegroot.nl.gpg.key -O - | apt-key add -

apt-get update
apt-get -y install git-core binutils ca-certificates
wget https://raw.github.com/Hexxeh/rpi-update/master/rpi-update -O /usr/bin/rpi-update
chmod +x /usr/bin/rpi-update
mkdir /lib/modules
rpi-update

# Install packages
apt-get -y install locales ntp openssh-server sudo wireless-tools wpasupplicant bash-completion

# Create user
echo \"root:rpi\" | chpasswd

# Network
sed -i -e 's/KERNEL\!=\"eth\*|/KERNEL\!=\"/' /lib/udev/rules.d/75-persistent-net-generator.rules
rm -f /etc/udev/rules.d/70-persistent-net.rules

# Set time zone
rm /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
echo Europe/Amsterdam > /etc/timezone

# Configure locales
dpkg-reconfigure locales tzdata

rm -f third-stage
" > third-stage
chmod +x third-stage
LANG=C chroot ${rootfs} /third-stage

touch boot/wifi-settings
echo "network={
	ssid=\"SSID.NAME\"
	psk=\"PSK.KEY\"
}" > boot/wifi-settings
ln -s /boot/wifi-settings etc/wpa_supplicant/wpa_supplicant.conf

echo "#!/bin/bash
apt-get update
apt-get clean
rm -f cleanup
service ssh stop
rm /usr/bin/qemu-arm-static
" > cleanup
chmod +x cleanup
LANG=C chroot ${rootfs} /cleanup

cd

umount ${bootfs}
umount ${rootfs}

if [ "${image}" != "" ]; then
  kpartx -d ${image}
  echo "Created image ${image}"
fi

if [ -d "${rootfs}" ]; then
  rm -rf ${rootfs}
fi

echo "done."

